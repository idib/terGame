package lab6;

import lab6.distributions.distribution;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by idib on 21.05.17.
 */
public class Model {
    private int state;
    private double[] timeState;
    private int countTread;
    private int sizeQueue;
    private distribution admission;
    private distribution service;
    private double startTime;
    private double endTime;
    private double curTime;

    private ArrayList<Double> times;
    private Queue<request> queue;
    private ArrayList<request> requests;

    public Model(int countTread, int sizeQueue, distribution admission, distribution service, double startTime, double endTime) {
        this.countTread = countTread;
        this.sizeQueue = sizeQueue;
        this.admission = admission;
        this.service = service;
        this.startTime = startTime;
        this.endTime = endTime;
        times = new ArrayList<>();
        queue = new LinkedList<>();
        requests = new ArrayList<request>();
        timeState = new double[1 + countTread + sizeQueue];
    }

    private void generateReceived() {
        double time = startTime;
        while (time < endTime) {
            time += admission.get();
            requests.add(new request(time));
        }
    }

    public statistic getStatistic() {
        statistic stat = new statistic();
        int n = requests.size();
        for (request curRequest : requests)
            stat.avgWaitTime += curRequest.timeBegin - curRequest.timeCreate;
        stat.avgWaitTime /= n;
        for (request curRequest : requests)
            stat.avgServTime += curRequest.timeClose - curRequest.timeBegin;
        stat.avgServTime /= n;
        stat.avgCountEvt = n;
        for (request curRequest : requests)
            stat.nChance += curRequest.miss ? 1 : 0;
        stat.nChance /= n;
        stat.nLamAvg = n / (endTime - startTime);
        stat.nQ = 1 - stat.nChance;

        for (int i = 0; i < timeState.length; i++) {
            timeState[i] /= endTime - startTime;
        }
        return stat;
    }

    public void start() {
        state = 0;
        curTime = startTime;
        requests.clear();
        queue.clear();
        times.clear();
        generateReceived();
        for (int i = 0; i < requests.size(); ) {
            double lastTime = curTime;
            request curRequest = requests.get(i);
            curTime = curRequest.timeBegin;

            Collections.sort(times);
            for (int j = 0; j < times.size(); j++) {
                double time = times.get(j);
                if (time < curTime)
                    if (!queue.isEmpty()) {
                        request req = queue.poll();
                        req.timeBegin = time;
                        req.timeClose = time + service.get();
                        timeState[state--] += time - lastTime;
                        lastTime = time;
                        times.set(j, req.timeClose);
                    } else {
                        times.remove(time);
                        j--;
                    }
            }

            if (times.size() <= countTread) {
                curRequest.timeBegin = curTime;
                curRequest.timeClose = curTime + service.get();
                timeState[state++] += curTime - lastTime;
                times.add(curRequest.timeClose);
            } else if (queue.size() <= sizeQueue) {
                queue.add(curRequest);
                timeState[state++] += curTime - lastTime;
            } else
                curRequest.miss = true;
        }
//        timeState[state] = endTime - curTime; // ???
    }

    class statistic {
        double nLamAvg;
        double nChance;
        double nQ;
        double nR;
        double avgWaitTime;
        double avgServTime;
        double avgCountEvt;
    }

    class request {
        double timeCreate;
        double timeBegin;
        double timeClose;
        boolean miss;

        public request(double timeCreate) {
            this.timeCreate = timeCreate;
        }
    }
}
