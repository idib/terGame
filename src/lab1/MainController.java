//package lab2;
//
//import javafx.application.Application;
//import javafx.collections.FXCollections;
//import javafx.embed.swing.SwingNode;
//import javafx.event.ActionEvent;
//import javafx.fxml.FXML;
//import javafx.fxml.FXMLLoader;
//import javafx.scene.Parent;
//import javafx.scene.Scene;
//import javafx.scene.control.Label;
//import javafx.scene.control.TableColumn;
//import javafx.scene.control.TableView;
//import javafx.scene.control.cell.PropertyValueFactory;
//import javafx.scene.layout.AnchorPane;
//import javafx.stage.Stage;
//import org.jfree.data.gantt.XYTaskDataset;
//
//import javax.swing.*;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//
//public class MainController extends Application {
//    List<Works> WorksList2 = Arrays.asList(
//            new Works(1, 9, 3),
//            new Works(2, 7, 4),
//            new Works(3, 6, 3),
//            new Works(4, 2, 6),
//            new Works(5, 3, 6)
//    );
//
//    List<Works> WorksList3 = Arrays.asList(
//            new Works(1, 9, 7, 9),
//            new Works(2, 6, 4, 14),
//            new Works(3, 7, 3, 11),
//            new Works(4, 3, 5, 10),
//            new Works(5, 2, 8, 8)
//    );
//
//
//    @FXML
//    private SwingNode chartsCanvas;
//
//    @FXML
//    private TableView tab1;
//
//    @FXML
//    private TableView tab2;
//
//    @FXML
//    private Label labX;
//
//    @FXML
//    private Label labT;
//
//    @FXML
//    private Label nlabX;
//
//    @FXML
//    private Label nlabT;
//
//    public static void main(String[] args) {
//        launch(args);
//    }
//
//    @Override
//    public void start(Stage primaryStage) throws Exception {
//        Parent root = null;
//        try {
//            root = FXMLLoader.load(getClass().getResource("lab1/MainView.fxml"));
//            AnchorPane r = new AnchorPane(root);
//            AnchorPane.setBottomAnchor(root, 0.);
//            AnchorPane.setLeftAnchor(root, 0.);
//            AnchorPane.setRightAnchor(root, 0.);
//            AnchorPane.setTopAnchor(root, 0.);
//            primaryStage.setTitle("MMOH");
//            primaryStage.setScene(new Scene(r, 1500, 700));
//            primaryStage.show();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void Runs(ActionEvent actionEvent) {
//        List<Works> a = order(WorksList2);
//        List<Works> b = Optimize2(WorksList2);
//
//        TableColumn f = new TableColumn("№");
//        f.setCellValueFactory(new PropertyValueFactory<>("valI"));
//
//        TableColumn s = new TableColumn("ai");
//        s.setCellValueFactory(new PropertyValueFactory<>("valA"));
//
//        TableColumn t = new TableColumn("bi");
//        t.setCellValueFactory(new PropertyValueFactory<>("valB"));
//
//        tab1.getColumns().clear();
//        tab1.getColumns().addAll(f, s, t);
//        tab1.setItems(FXCollections.observableArrayList(a));
//
//        f = new TableColumn("№");
//        f.setCellValueFactory(new PropertyValueFactory<>("valI"));
//
//        s = new TableColumn("ai");
//        s.setCellValueFactory(new PropertyValueFactory<>("valA"));
//
//        t = new TableColumn("bi");
//        t.setCellValueFactory(new PropertyValueFactory<>("valB"));
//
//        tab2.getColumns().clear();
//        tab2.getColumns().addAll(f, s, t);
//        tab2.setItems(FXCollections.observableArrayList(b));
//
//        long acA = 0;
//        long acB = 0;
//        long X = 0;
//        for (int i = 0; i < b.size(); i++) {
//            acA += a.get(i).a;
//            if (i != 0)
//                acB += a.get(i - 1).b;
//            if (acA - acB > X)
//                X = acA - acB;
//        }
//
//        acB += a.get(a.size() - 1).b + X;
//
//        labX.setText("" + X);
//        labT.setText("" + acB);
//
//        acA = acB = X = 0;
//        for (int i = 0; i < b.size(); i++) {
//            acA += b.get(i).a;
//            if (i != 0)
//                acB += b.get(i - 1).b;
//            if (acA - acB > X)
//                X = acA - acB;
//        }
//
//        acB += b.get(b.size() - 1).b + X;
//
//        nlabX.setText("" + X);
//        nlabT.setText("" + acB);
//
//        b.sort((o1, o2) -> Integer.compare(o1.indexInOrder, o2.indexInOrder));
//
//        SwingUtilities.invokeLater(() -> chartsCanvas.setContent(
//                XYTaskDataset.createDemoPanel(a, b, 2)));
//
//    }
//
//    public List<Works> order(List<Works> l) {
//
//        long acum = 0;
//
//        for (int i = 0; i < l.size(); i++) {
//            l.get(i).timeA = acum;
//            acum += l.get(i).a;
//        }
//
//        acum = 0;
//        long acumB = l.get(0).a;
//        int n = l.size();
//        for (int i = 0; i < n; i++) {
//            acum += l.get(i).a;
//
//
//            l.get(i).timeB = Math.max(acum, acumB);
//            acumB = l.get(i).timeB + l.get(i).b;
//        }
//
//
//        acumB = 0;
//        long acumC = l.get(0).b + l.get(0).timeB;
//
//        for (int i = 0; i < n; i++) {
//            acumB += l.get(i).b;
//
//
//            l.get(i).timeC = Math.max(acumB, acumC);
//            acumC = l.get(i).timeC + l.get(i).c;
//        }
//        return l;
//    }
//
//    private List<Works> Optimize2(List<Works> l) {
//        ArrayList<Works> r = new ArrayList<>();
//        for (Works works : l) {
//            r.add(works.clone());
//        }
//        l = r;
//
//        sortOpt(l);
//
//        order(l);
//
//        System.out.println(l);
//        return l;
//    }
//
//
//    private List<Works> Optimize3(List<Works> l) {
//        ArrayList<Works> r = new ArrayList<>();
//
//        long minA, maxB, minC;
//        minA = l.get(0).a;
//        maxB = l.get(0).b;
//        minC = l.get(0).c;
//        for (Works works : l) {
//            if (works.a < minA)
//                minA = works.a;
//            if (works.b > maxB)
//                maxB = works.b;
//            if (works.c < minC)
//                minC = works.c;
//        }
//
//        if (minA >= maxB || minC >= maxB) {
//            for (Works works : l) {
//                r.add(works.clone());
//            }
//            l = r;
//
//            sortOpt(l);
//
//            order(l);
//
//            System.out.println(l);
//        }
//
//        return l;
//    }
//
//
//    private void sortOpt(List<Works> l) {
//
//        l.sort(
//                (o1, o2) -> {
//                    int a1, a2, b1, b2;
//                    if (o1.to) {
//                        a1 = o1.a;
//                        b1 = o1.b;
//                        a2 = o2.a;
//                        b2 = o2.b;
//                    } else {
//                        a1 = o1.a + o1.b;
//                        b1 = o1.b + o1.c;
//                        a2 = o2.a + o2.b;
//                        b2 = o2.b + o2.c;
//                    }
//                    if (b1 > a1 && b2 > a2) {
//                        if (a1 != a2)
//                            return Integer.compare(a1, a2);
//                        else
//                            return Integer.compare(b2, b1);
//                    }
//                    if (b1 < a1 && b2 < a2) {
//                        if (a1 != a2)
//                            return Integer.compare(b2, b1);
//                        else
//                            return Integer.compare(a1, a2);
//                    } else {
//                        if (b1 == b2 && a1 == a2)
//                            return 0;
//                        if (b1 >= a1)
//                            return -1;
//                        else
//                            return 1;
//                    }
//                });
//    }
//
//    public void Runs3(ActionEvent actionEvent) {
//        List<Works> a = order(WorksList3);
//        List<Works> b = Optimize3(WorksList3);
//
//        TableColumn f = new TableColumn("№");
//        f.setCellValueFactory(new PropertyValueFactory<>("valI"));
//
//        TableColumn s = new TableColumn("ai");
//        s.setCellValueFactory(new PropertyValueFactory<>("valA"));
//
//        TableColumn t = new TableColumn("bi");
//        t.setCellValueFactory(new PropertyValueFactory<>("valB"));
//
//
//        TableColumn fo = new TableColumn("ci");
//        fo.setCellValueFactory(new PropertyValueFactory<>("valC"));
//
//        tab1.getColumns().clear();
//        tab1.getColumns().addAll(f, s, t, fo);
//        tab1.setItems(FXCollections.observableArrayList(a));
//
//        f = new TableColumn("№");
//        f.setCellValueFactory(new PropertyValueFactory<>("valI"));
//
//        s = new TableColumn("ai");
//        s.setCellValueFactory(new PropertyValueFactory<>("valA"));
//
//        t = new TableColumn("bi");
//        t.setCellValueFactory(new PropertyValueFactory<>("valB"));
//
//        fo = new TableColumn("ci");
//        fo.setCellValueFactory(new PropertyValueFactory<>("valC"));
//
//        tab2.getColumns().clear();
//        tab2.getColumns().addAll(f, s, t, fo);
//        tab2.setItems(FXCollections.observableArrayList(b));
//
////        long acA = 0;
////        long acB = 0;
////        long X = 0;
////        for (int i = 0; i < b.size(); i++) {
////            acA += a.get(i).a;
////            if (i != 0)
////                acB += a.get(i - 1).b;
////            if (acA - acB > X)
////                X = acA - acB;
////        }
////
////        acB += a.get(a.size() - 1).b + X;
////
////        labX.setText("" + X);
////        labT.setText("" + acB);
////
////        acA = acB = X = 0;
////        for (int i = 0; i < b.size(); i++) {
////            acA += b.get(i).a;
////            if (i != 0)
////                acB += b.get(i - 1).b;
////            if (acA - acB > X)
////                X = acA - acB;
////        }
////
////        acB += b.get(b.size() - 1).b + X;
////
////        nlabX.setText("" + X);
////        nlabT.setText("" + acB);
//
//        b.sort((o1, o2) -> Integer.compare(o1.indexInOrder, o2.indexInOrder));
//
//        SwingUtilities.invokeLater(() -> chartsCanvas.setContent(
//                XYTaskDataset.createDemoPanel(a, b, 3)));
//    }
//}