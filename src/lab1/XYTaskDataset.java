////
//// Source code recreated from a .class file by IntelliJ IDEA
//// (powered by Fernflower decompiler)
////
//
//import org.jfree.chart.ChartPanel;
//import org.jfree.chart.JFreeChart;
//import org.jfree.chart.axis.DateAxis;
//import org.jfree.chart.axis.SymbolAxis;
//import org.jfree.chart.labels.ItemLabelAnchor;
//import org.jfree.chart.labels.ItemLabelPosition;
//import org.jfree.chart.plot.CombinedDomainXYPlot;
//import org.jfree.chart.plot.XYPlot;
//import org.jfree.chart.renderer.xy.XYBarRenderer;
//import org.jfree.data.gantt.Task;
//import org.jfree.data.gantt.TaskSeries;
//import org.jfree.data.gantt.TaskSeriesCollection;
//import org.jfree.data.time.SimpleTimePeriod;
//import org.jfree.data.xy.IntervalXYDataset;
//import org.jfree.ui.ApplicationFrame;
//import org.jfree.ui.RefineryUtilities;
//import org.jfree.ui.TextAnchor;
//
//import javax.swing.*;
//import java.awt.*;
//import java.util.List;
//
//public class XYTaskDataset extends ApplicationFrame {
//    public XYTaskDataset(String var1) {
//        super(var1);
////        JPanel var2 = createDemoPanel();
////        var2.setPreferredSize(new Dimension(500, 300));
////        this.setContentPane(var2);
//    }
//
//    private static XYPlot createSubplot(IntervalXYDataset var0, int c) {
//        DateAxis var1 = new DateAxis("Date/Time");
//        SymbolAxis var2;
//        if (c == 2)
//            var2=new SymbolAxis("Resources", new String[]{"Team B", "Team A"});
//        else
//            var2=new SymbolAxis("Resources", new String[]{"Team C","Team B", "Team A"});
//        var2.setGridBandsVisible(false);
//        XYBarRenderer renderer = new XYBarRenderer();
//
//        renderer.setBaseItemLabelGenerator((xyDataset, i, j) ->
//                "task №" + (j + 1)
//        );
//        renderer.setBaseItemLabelsVisible(true);
//        renderer.setBaseItemLabelPaint(Color.BLACK);
//
//        Color borderColor = Color.BLACK;
//        renderer.setDrawBarOutline(true);
//        renderer.setSeriesOutlinePaint(3, borderColor);
//
//        renderer.setShadowVisible(false);
//
//        renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(
//                ItemLabelAnchor.INSIDE6, TextAnchor.BOTTOM_CENTER));
//
//        renderer.setUseYInterval(true);
//        XYPlot var4 = new XYPlot(var0, var1, var2, renderer);
//        return var4;
//    }
//
//    private static JFreeChart createChart(List<Works> a, List<Works> b, int c) {
//
//        CombinedDomainXYPlot var0 = new CombinedDomainXYPlot(new DateAxis("Date/Time"));
//        var0.setDomainPannable(true);
//        var0.add(createSubplot(createDataset(a), c));
//        var0.add(createSubplot(createDataset(b), c));
//
//
//        JFreeChart var1 = new JFreeChart("XYTaskDatasetDemo2", var0);
//        return var1;
//    }
//
//    public static JPanel createDemoPanel(List<Works> a, List<Works> b, int c) {
//        return new ChartPanel(createChart(a, b, c));
////        JFreeChart var0 = createChart(createDataset());
////        ChartPanel var1 = new ChartPanel(var0);
////        var1.setMouseWheelEnabled(true);
////        return var1;
//    }
//
//    private static IntervalXYDataset createDataset(List<Works> a) {
//        org.jfree.data.gantt.XYTaskDataset var0 = new org.jfree.data.gantt.XYTaskDataset(createTasks(a));
//        var0.setTransposed(true);
//        var0.setSeriesWidth(0.6D);
//        return var0;
//    }
//
//    private static TaskSeriesCollection createTasks(List<Works> a) {
//        TaskSeriesCollection var0 = new TaskSeriesCollection();
//        TaskSeries var1 = new TaskSeries("Team B");
//        TaskSeries var2 = new TaskSeries("Team A");
//        TaskSeries var3 = new TaskSeries("Team C");
//        for (Works works : a) {
//
//            var1.add(new Task(
//                    "T1" + works.indexInOrder,
//                    new SimpleTimePeriod(works.timeB * 1000, (works.b + works.timeB) * 1000)));
//            var2.add(new Task(
//                    "T2" + works.indexInOrder,
//                    new SimpleTimePeriod(works.timeA * 1000, (works.a + works.timeA) * 1000)));
//        }
//
//        if (!a.get(0).to)
//        {
//            for (Works works : a) {
//
//                var3.add(new Task(
//                        "T1" + works.indexInOrder,
//                        new SimpleTimePeriod(works.timeC * 1000, (works.c + works.timeC) * 1000)));
//            }
//            var0.add(var3);
//        }
//
//        var0.add(var1);
//        var0.add(var2);
//        return var0;
//    }
//
//    public static void main(String[] var0) {
//        XYTaskDataset var1 = new XYTaskDataset("JFreeChart : XYTaskDataset.java");
//        var1.pack();
//        RefineryUtilities.centerFrameOnScreen(var1);
//        var1.setVisible(true);
//    }
//}
