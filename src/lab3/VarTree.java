package lab3;

import javafx.scene.paint.Color;

import java.io.File;
import java.io.FileNotFoundException;
import java.security.PublicKey;

/**
 * Created by idib on 20.09.16.
 */
public class VarTree<K extends Comparable<K>, V> {
    private TSP Root;

    private double distX = 70;
    private double distY = 45;
    private double sX = 0;
    private double sY = 0;

    public VarTree(File file) throws FileNotFoundException {
        Root = new TSP(file);
        Root.Start();
    }

    public TSP getRoot() {
        return Root;
    }

    public void refreshXY() {
        if (Root != null) {
            refreshX(Root, sX);
            refreshY(Root, sY);
        }
    }

    private double refreshX(TSP n, double curX) {
        double s = curX;
        if (n.TryLeft())
            s = refreshX(n.getLeft(), s);
        n.setX(s);
        if (n.TryRight())
            return refreshX(n.getRight(), s + distX);
        else
            return s + distX;
    }

    private void refreshY(TSP n, double curY) {
        n.setY(curY);
        if (n.TryLeft())
            refreshY(n.getLeft(), curY + distY);
        if (n.TryRight())
            refreshY(n.getRight(), curY + distY);
    }

    public double getDistX() {
        return distX - 2 * TSP.Radius;
    }

    public void setDistX(double distX) {
        this.distX = distX + 2 * TSP.Radius;
    }

    public double getDistY() {
        return distY - 2 * TSP.Radius;
    }

    public void setDistY(double distY) {
        this.distY = distY + 2 * TSP.Radius;
    }

    public double getSX() {
        return sX;
    }

    public void setSX(double sX) {
        this.sX = sX;
    }

    public double getSY() {
        return sY;
    }

    public void setSY(double sY) {
        this.sY = sY;
    }
}