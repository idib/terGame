package lab6.distributions;

import lab6.lab6;

import java.util.Random;

/**
 * Created by idib on 21.05.17.
 */
public class weibull implements distribution {
    private double k;
    private double lambda;
    private Random rand;

    public weibull(double k, double lambda, Random rand) {
        this.k = k;
        this.lambda = lambda;
        this.rand = rand;
    }

    @Override
    public double get() {
        return (-lambda * Math.pow(Math.log(rand.nextDouble()), 1 / k));
    }
}
