package lab6.distributions;

import java.util.Random;

/**
 * Created by idib on 21.05.17.
 */
public class exponential implements distribution {
    private double lambda;
    private Random rand;

    public exponential(double lambda, Random rand) {
        this.lambda = lambda;
        this.rand = rand;
    }

    @Override
    public double get() {
        return -Math.log(rand.nextDouble()) / lambda;
    }
}
