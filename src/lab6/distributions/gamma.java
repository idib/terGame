package lab6.distributions;

import java.util.Random;

/**
 * Created by idib on 21.05.17.
 */
public class gamma implements distribution {
    private double nu;
    private double lambda;
    private Random rand;

    public gamma(double nu, double lambda, Random rand) {
        this.nu = nu;
        this.lambda = lambda;
        this.rand = rand;
    }

    @Override
    public double get() {
        double t1 = 0;
        for (int i = 0; i < nu; i++) {
            t1 += Math.log(1 - rand.nextDouble());
        }
        return (-1 / lambda) * t1;
    }
}
