package lab3;

import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Line;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import javafx.util.Pair;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

/**
 * Created by idib on 02.04.17.
 */
public class TSP {
    public static double Radius = 20;
    private static double Maxs;
    public String path;
    private List<Pair<Integer, Integer>> pahtPar = new ArrayList<>();
    private double minimuMinimore = 0;
    private int[] NameCol;
    private int[] NameRow;
    private double[][] input;
    private double[][] matrix;
    private int from;
    private int to;
    private double curLen = -2;
    private TSP right, left;
    private int n;
    private double X;
    private double Y;
    private boolean isLeft;


    TSP(TSP r) {
        input = r.input;
        n = r.n;
        minimuMinimore = r.minimuMinimore;
        matrix = new double[n][n];
        NameCol = new int[n];
        NameRow = new int[n];
        for (int i = 0; i < n; i++) {
            NameCol[i] = r.NameCol[i];
            NameRow[i] = r.NameRow[i];
            for (int j = 0; j < n; j++)
                matrix[i][j] = r.matrix[i][j];
        }
        pahtPar = r.pahtPar;
    }

    public TSP(File file) throws FileNotFoundException {
        readFile(file);
    }

    private static void setInf(TSP r, int s, int e) {
        int i, j;
        i = j = r.n - 1;
        while (i > 0 && r.NameRow[i] != s) i--;
        while (j > 0 && r.NameCol[j] != e) j--;

        if (i != -1 && j != -1)
            r.matrix[i][j] = Double.POSITIVE_INFINITY;
    }

    private Text GetName() {
        Text f = new Text();
        f.setText((isLeft ? "!" : "") + (from + 1) + ' ' + (to + 1) + "     " + minimuMinimore + ".." + Maxs);
        f.setX(X - Radius / 2);
        f.setY(Y);
        f.setOnMouseEntered(event -> print());
        return f;
    }

    private Ellipse getEllipse() {
        Ellipse s = new Ellipse();
        s.setCenterX(X);
        s.setCenterY(Y);
        s.setRadiusX(TSP.Radius);
        s.setRadiusY(TSP.Radius);
        s.setFill(Color.TRANSPARENT);
        s.setStroke(Color.BLACK);
        s.setOnMouseEntered(event -> print());
        return s;
    }

    private void getInfs(TSP r, Pair<Integer, Integer> p) {
        if (r.n >= 2) {
            for (Pair<Integer, Integer> pair : r.pahtPar) {
                if (Objects.equals(pair.getKey(), p.getValue())) {
                    setInf(r, pair.getValue(), p.getKey());
                    setInf(r, p.getKey(), pair.getValue());
                }
                if (Objects.equals(pair.getValue(), p.getKey())) {
                    setInf(r, pair.getKey(), p.getValue());
                    setInf(r, p.getValue(), pair.getKey());
                }
            }
            r.pahtPar.add(p);
        }
    }

    private ArrayList<Line> getLP(double sX, double sY, double eX, double eY) {
        ArrayList<Line> res = new ArrayList<>();
        double deg = Math.atan2(eY - Y, eX - X);
        double neX = TSP.Radius * Math.cos(deg);
        double neY = TSP.Radius * Math.sin(deg);
        res.add(new Line(eX - neX, eY - neY, sX + neX, sY + neY));

        deg -= Math.PI / 6;
        double nX = TSP.Radius * Math.cos(deg);
        double nY = TSP.Radius * Math.sin(deg);
        res.add(new Line(eX - neX - nX, eY - neY - nY, eX - neX, eY - neY));

        deg += Math.PI / 3;
        nX = TSP.Radius * Math.cos(deg);
        nY = TSP.Radius * Math.sin(deg);
        res.add(new Line(eX - neX - nX, eY - neY - nY, eX - neX, eY - neY));
        return res;
    }

    private ArrayList<Line> getLinePoint() {
        ArrayList<Line> res = new ArrayList<>();
        if (TryLeft())
            res.addAll(getLP(X, Y, left.X, left.Y));
        if (TryRight())
            res.addAll(getLP(X, Y, right.X, right.Y));
        return res;
    }

    private List<Pair<Integer, Integer>> getList() {
        List<Pair<Integer, Integer>> res = new ArrayList<>();
        res.add(new Pair<>(from, to));
        if (curLen == left.curLen)
            res.addAll(left.getList());
        if (curLen == right.curLen)
            res.addAll(right.getList());
        return res;
    }

    private TSP getSub(int si, int sj) {
        TSP r = new TSP(this);
        r.from = NameRow[si];
        r.to = NameCol[sj];
        r.matrix = new double[n - 1][n - 1];
        r.n = n - 1;
        int i = 0, j = 0;
        r.NameRow = new int[n - 1];
        r.NameCol = new int[n - 1];
        for (int qi = 0; qi < n; qi++) {
            if (qi != sj) {
                r.NameCol[i] = NameCol[qi];
                i++;
            }
        }
        i = 0;
        for (int qi = 0; qi < n; qi++) {
            j = 0;
            if (qi != si) {
                r.NameRow[i] = NameRow[qi];
                for (int qj = 0; qj < n; qj++) {
                    if (qj != sj) {
                        r.matrix[i][j] = matrix[qi][qj];
                        j++;
                    }
                }
                i++;
            }
        }

        for (int qi = 0; qi < n - 1; qi++) {
            for (int qj = 0; qj < n - 1; qj++) {
                if (r.NameRow[qi] == NameCol[sj] && NameCol[qj] == NameRow[si])
                    r.matrix[qi][qj] = Double.POSITIVE_INFINITY;
            }
        }

        r.minimuMinimore += r.minimize();
        getInfs(r, new Pair<>(NameRow[si], NameCol[sj]));
        return r;
    }

    private TSP getWithInf(int si, int sj) {
        TSP r = new TSP(this);
        r.matrix[si][sj] = Double.POSITIVE_INFINITY;
        r.from = NameRow[si];
        r.to = NameCol[sj];
        return r;
    }

    private double minimize() {
        double min = 0;
        double sum = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j == 0)
                    min = matrix[i][j];
                min = Math.min(matrix[i][j], min);
            }
            if (min != Double.POSITIVE_INFINITY) {
                sum += min;
                for (int j = 0; j < n; j++) {
                    matrix[i][j] -= min;
                }
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j == 0)
                    min = matrix[j][i];
                min = Math.min(matrix[j][i], min);
            }
            if (min != Double.POSITIVE_INFINITY) {
                sum += min;
                for (int j = 0; j < n; j++) {
                    matrix[j][i] -= min;
                }
            }
        }
        return sum;
    }

    private double next() {
        if (minimuMinimore >= Maxs || n <= 1)
            return minimuMinimore;
        double max = Double.NEGATIVE_INFINITY;
        int si = -1, sj = -1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] == 0 && matrix[i][j] != Double.POSITIVE_INFINITY) {
                    double temp = teta(i, j);
                    if (temp > max) {
                        si = i;
                        sj = j;
                        max = temp;
                    }
                }
            }
        }
        if (si == -1 || sj == -1 || matrix[si][sj] == Double.POSITIVE_INFINITY)
            return Double.POSITIVE_INFINITY;
        right = getSub(si, sj);
        right.minimuMinimore += matrix[si][sj];
        left = getWithInf(si, sj);
        left.isLeft = true;
        left.minimuMinimore += left.minimize();
        curLen = Math.min(left.next(),right.next());
        return curLen;
    }

    private void print() {
        System.out.println("\n\n\n\n\n\n");
        System.out.println((isLeft ? "!" : "") + (from + 1) + ' ' + (to + 1));
        System.out.println(minimuMinimore);
        for (int i = -1; i < n; i++) {
            if (i != -1) {
                System.out.print(NameRow[i] + 1 + " ");
                for (int j = 0; j < n; j++) {
                    if (matrix[i][j] != Double.POSITIVE_INFINITY)
                        System.out.print(matrix[i][j] + " ");
                    else
                        System.out.print("+OO ");
                }
            } else {
                System.out.print("  ");
                for (int j = 0; j < n; j++) {
                    System.out.print(NameCol[j] + 1 + " ");
                }
            }
            System.out.println();
        }
    }

    private void readFile(File file) throws FileNotFoundException {
        Scanner in = new Scanner(file);
        n = in.nextInt();
        NameCol = new int[n];
        NameRow = new int[n];
        input = new double[n][n];
        matrix = new double[n][n];
        for (int i = 0; i < n; i++) {
            NameCol[i] = NameRow[i] = i;
            for (int j = 0; j < n; j++) {
                if (i != j)
                    matrix[i][j] = in.nextDouble();
                else
                    matrix[i][j] = Double.POSITIVE_INFINITY;
                input[i][j] = matrix[i][j];
            }
        }
    }

    private double teta(int i, int j) {
        double minR = Double.POSITIVE_INFINITY, minC = Double.POSITIVE_INFINITY;
        for (int k = 0; k < n; k++) {
            if (j != k)
                minR = Math.min(minR, matrix[i][k]);
            if (i != k)
                minC = Math.min(minC, matrix[k][j]);
        }
        return minC + minR;
    }

    private double tryPlan() {
        double sum = 0;
        for (int i = 0; i < n; i++)
            sum += matrix[i][(i + 1) % n];
        Maxs = sum;
        return sum;
    }

    public void Start() {
        minimuMinimore = minimize();
        tryPlan();
        next();
        if (curLen >= Maxs) {
            path = "";
            for (int i = 0; i < n; i++) {
                path += i + " ";
            }
            path += " Dist: " + Maxs;
        } else {
//            List<Pair<Integer, Integer>> res = getList();
//            res.remove(new Pair<Integer, Integer>(0, 0));
//            List<Integer> r = new ArrayList<>();
//            int ind = res.get(0).getValue();
//            res.remove(0);
//            while (!res.isEmpty()) {
//                r.add(ind);
//                for (int i = 0; i < res.size(); i++) {
//                    if (res.get(i).getKey() == ind) {
//                        ind = res.get(i).getValue();
//                        res.remove(i);
//                        break;
//                    }
//                }
//            }
//
//            for (int i = 0; i < n; i++) {
//                path += r.get(i) + " ";
//            }
//
//            path += " Dist: " + curLen;
        }
    }

    public boolean TryLeft() {
        if (left != null)
            return true;
        return false;
    }

    public boolean TryRight() {
        if (right != null)
            return true;
        return false;
    }

    public TSP getLeft() {
        return left;
    }

    public TSP getRight() {
        return right;
    }

    public ArrayList<Shape> getShape() {
        ArrayList<Shape> res = new ArrayList<>();
        res.add(getEllipse());
        res.addAll(getLinePoint());
        res.add(GetName());
        return res;
    }

    public void setX(double x) {
        X = x;
    }

    public void setY(double y) {
        Y = y;
    }
}
