package lab7;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by idib on 23.05.17.
 */
public class lab7 {
    private static Double xi = 0.6;

    private static int findMax(int[] array, int size) {
        int max = array[0];
        for (int i = 1; i < size; i++) {
            if (max < array[i]) {
                max = array[i];
            }
        }
        return max;
    }

    // Функция по нахождению минимума
    private static int findMin(int[] array, int size) {
        int min = array[0];
        for (int i = 1; i < size; i++) {
            if (min > array[i]) {
                min = array[i];
            }
        }
        return min;
    }

    public static void main(String[] args) {
        task1();
        task2();
    }

    private static int[][] read(String s) {
        File r = new File(s);
        int n = 3, m = 4;
        int[][] arr = new int[n][m];
        Scanner in = null;
        try {
            in = new Scanner(r);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                arr[i][j] = in.nextInt();
            }
        }
        return arr;
    }


    private static double[] readV(String s) {
        File r = new File(s);

        int m = 4;
        double[] arr = new double[m];
        Scanner in = null;
        try {
            in = new Scanner(r);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        for (int j = 0; j < m; j++) {
            arr[j] = in.nextDouble();
        }
        return arr;
    }

    private static void task1() {
        //2 активных игрока
        //задание 1 - определить цену игры
        int a, b;
        int n = 3, m = 4;
        int optimA, optimB;
        //a - нижняя цена игры
        //b - верхняя цена игры
        int[] minForRows = new int[n];
        int[] maxForColumns = new int[m];
        int[][] mtr = new int[n][m];
        int[] playerAS = new int[n];    //номера стратегий игроков
        int[] playerBS = new int[m];
        //заполнить номера стратегий
        for (int i = 0; i < n; i++) {
            playerAS[i] = i + 1;
        }

        for (int i = 0; i < m; i++) {
            playerBS[i] = i + 1;
        }

        mtr = read("src/lab7/mat1");

        //нахождение максимина
        for (int i = 0; i < n; i++) {
            int rMin = Integer.MAX_VALUE;
            for (int j = 0; j < m; j++) {
                if (mtr[i][j] < rMin) {
                    rMin = mtr[i][j];
                }
            }
            minForRows[i] = rMin;
        }
        a = minForRows[0];
        optimA = 0;
        for (int i = 1; i < n; i++) {
            if (a < minForRows[i]) {
                a = minForRows[i];
                optimA = i;
            }
        }

        //нахождение минимакса
        for (int j = 0; j < m; j++) {
            int rMax = Integer.MIN_VALUE;
            for (int i = 0; i < n; i++) {
                if (mtr[i][j] > rMax) {
                    rMax = mtr[i][j];
                }
            }
            maxForColumns[j] = rMax;
        }
        optimB = 0;
        b = maxForColumns[0];
        for (int j = 1; j < m; j++) {
            if (b > maxForColumns[j]) {
                b = maxForColumns[j];
                optimB = j;
            }
        }

        //проверка наличия седловых точек
        if (a == b) {
            //решение игры найдено
            //седловая точка

            System.out.println("Матрица игры имеет седловые точки");
            System.out.println("Цена игры: " + a);
            System.out.println("Стратегии, соответствующие цене игры:");
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    if (mtr[i][j] == a) {
                        System.out.println("A" + (i + 1) + ", B" + (j + 1));
                    }
                }
            }

            System.out.println("==================");

        }
        //точная верхняя цена игры
        System.out.println("Нижняя цена игры: " + a);
        System.out.println("Верхняя цена игры: " + b);


        //решение игры методом итераций
        int[] strA, strB;
        strA = new int[n];
        strB = new int[m];
        double vmin, vmax, vavg;
        double eps = 0.05;
        int itCount = 1;
        int maxItCount = 10000;
        ArrayList<Integer> listA, listB;
        listA = new ArrayList<>();
        listB = new ArrayList<>();
        //шаг 0 - предварительный выбор стратегий
        //игрок А выбирает ту  стратегию, которая обращает накопленный выигрыш в максимум
        int indexA = -1;
        for (int i = 0; i < n; i++) {
            int maxA = Integer.MIN_VALUE;
            for (int j = 0; j < n; j++) {
                if (mtr[i][j] > maxA) {
                    maxA = mtr[i][j];
                    indexA = i;
                }
            }
        }
        for (int i = 0; i < m; i++) {
            strB[i] += mtr[indexA][i];
        }
        listA.add(indexA);
        //игрок В выбирает ту стратегия, которая обращает накопленный выигрыш в минимум
        int indexB = -1;
        int minB = Integer.MAX_VALUE;
        for (int j = 0; j < m; j++) {
            if (mtr[indexA][j] < minB) {
                minB = mtr[indexA][j];
                indexB = j;
            }
        }
        for (int i = 0; i < n; i++) {
            strA[i] += mtr[i][indexB];
        }
        listB.add(indexB);
        vmin = (double) findMin(strB, m) / itCount;
        vmax = (double) findMax(strA, n) / itCount;
        vavg = (vmin + vmax) / 2;

        while (vmax - vmin > eps && itCount < maxItCount) {
            itCount++;
            int maxA = strA[0];
            indexA = 0;
            for (int i = 1; i < n; i++) {
                if (strA[i] > maxA) {
                    maxA = strA[i];
                    indexA = i;
                }
            }
            listA.add(indexA);
            for (int i = 0; i < m; i++) {
                strB[i] += mtr[indexA][i];
            }
            minB = strB[0];
            indexB = 0;
            for (int j = 1; j < m; j++) {
                if (strB[j] < minB) {
                    minB = strB[j];
                    indexB = j;
                }
            }
            listB.add(indexB);
            for (int i = 0; i < n; i++) {
                strA[i] += mtr[i][indexB];
            }
            vmin = (double) findMin(strB, m) / itCount;
            vmax = (double) findMax(strA, n) / itCount;
            vavg = (vmin + vmax) / 2;
        }

        //нахождение вероятностей
        double[] pa = new double[n];
        double[] pb = new double[m];
        for (int i = 0; i < listA.size(); i++) {
            pa[listA.get(i)] += 1;
        }
        for (int i = 0; i < listB.size(); i++) {
            pb[listB.get(i)] += 1;
        }
        for (int i = 0; i < n; i++) {
            pa[i] /= itCount;
        }
        for (int i = 0; i < m; i++) {
            pb[i] /= itCount;
        }

        System.out.println("Цена игры: " + vavg);
        System.out.println("Число итераций: " + itCount);
        System.out.println("Вероятности стратегий игрока А:");
        for (int i = 0; i < n; i++) {
            System.out.println("A" + (i + 1) + ": " + pa[i]);
        }
        System.out.println("Вероятности стратегий игрока B:");
        for (int i = 0; i < m; i++) {
            System.out.println("B" + (i + 1) + ": " + pb[i]);
        }
        System.out.println("==================");

        //упрощение игры
        boolean[] numbersSSA = new boolean[n];
        for (int i = 0; i < n; i++) {
            numbersSSA[i] = false;
        }

        //найти дублирующие и заведомо невыгодные стратегии
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i != j) {
                    boolean same = true;
                    for (int k = 0; k < m; k++) {
                        if (mtr[i][k] > mtr[j][k]) {
                            same = false;
                            break;
                        }
                    }
                    if (same) {
                        numbersSSA[i] = true;
                    }
                }

            }
        }

        //удаление дублирующих для игрока А
        int matrixHeigth = 0;
        for (int i = 0; i < n; i++) {
            if (!numbersSSA[i]) {
                matrixHeigth++;
            }
        }
        if (matrixHeigth < n) {
            //создание новой матрицы
            int[][] matrix1 = new int[matrixHeigth][m];
            int[] playerASCopy = new int[matrixHeigth];
            int k = 0;
            for (int i = 0; i < n; i++) {
                if (!numbersSSA[i]) {
                    System.arraycopy(mtr[i], 0, matrix1[k], 0, m);
                    playerASCopy[k] = playerAS[i];
                    k++;
                }
            }
            mtr = matrix1;
            n = matrixHeigth;
            playerAS = playerASCopy;
        }

        //нахождение дублирующих и заведомо невыгодные для В
        boolean[] numbersSSB = new boolean[m];
        for (int i = 0; i < m; i++) {
            numbersSSB[i] = false;
        }
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++) {
                if (i != j) {
                    boolean same = true;
                    for (int k = 0; k < n; k++) {
                        if (mtr[k][i] < mtr[k][j]) {
                            same = false;
                            break;
                        }
                    }
                    if (same) {
                        numbersSSB[i] = true;
                    }
                }
            }
        }

        //удаление дублирующих для игрока В
        int matrixWidth = 0;
        for (int i = 0; i < m; i++) {
            if (!numbersSSB[i]) {
                matrixWidth++;
            }
        }
        if (matrixWidth < m) {
            //создание новой матрицы
            int[][] matrix1 = new int[n][matrixWidth];
            int[] playerBSCopy = new int[matrixWidth];
            int k = 0;
            for (int j = 0; j < m; j++) {
                if (!numbersSSB[j]) {
                    for (int i = 0; i < n; i++) {
                        matrix1[i][k] = mtr[i][j];
                    }
                    playerBSCopy[k] = playerBS[j];
                    k++;
                }
            }
            mtr = matrix1;
            m = matrixWidth;
            playerBS = playerBSCopy;
        }
        //матрица упрощена
        //если n=2 или m=2, можно решить графически
                    /*if(n==2 && m!=2){
                        GraphForm gf=new GraphForm(this,false);
		                gf.setLocation(this.getX()+this.getWidth(), this.getY());
		                gf.show(mtr, 0, playerAS,playerBS,m);
		                gf.setVisible(true);
		            }
		            if(m==2 && n!=2){
		                GraphForm gf=new GraphForm(this,false);
		                gf.setLocation(this.getX()+this.getWidth(), this.getY());
		                gf.show(mtr, 1, playerAS,playerBS,n);
		                gf.setVisible(true);
		            }*/
        if (n == 2 && m == 2) {
            //решение можно найти однозначно
            double denom = mtr[0][0] + mtr[1][1] - mtr[0][1] - mtr[1][0];

            double p1 = (mtr[1][1] - mtr[1][0]) / denom;
            double p2 = 1 - p1;
            double v = (mtr[1][1] * mtr[0][0] - mtr[1][0] * mtr[0][1]);
            v /= denom;
            double q1 = (mtr[1][1] - mtr[0][1]);
            q1 /= denom;
            double q2 = 1 - q1;

            System.out.println("Матрица упрощена до 2х2");
            System.out.println("A" + playerAS[0] + ": " + p1);
            System.out.println("A" + playerAS[1] + ": " + p2);
            System.out.println("B" + playerBS[0] + ": " + q1);
            System.out.println("B" + playerBS[1] + ": " + q2);
            System.out.println("Цена игры: " + v);
            System.out.println("==================");
        }
    }

    private static void task2() {
        //игра с природой
        int n = 3, m = 4;
        int[][] mtr = new int[n][m];
        double[] q = new double[m];

        mtr = read("src/lab7/mat2");

        q = readV("src/lab7/mat3");

        //построить матрицу рисков
        int[] maxForColumns = new int[m];
        for (int j = 0; j < m; j++) {
            int max = mtr[0][j];
            for (int i = 1; i < n; i++) {
                if (max < mtr[i][j]) {
                    max = mtr[i][j];
                }
            }
            maxForColumns[j] = max;
        }
        int[][] risk = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                risk[i][j] = maxForColumns[j] - mtr[i][j];
            }
        }
        //рассчитать максимальное мат. ожидание выигрыша и средний риск
        double[] avgRisk = new double[n];
        double[] expValues = new double[n];
        for (int i = 0; i < n; i++) {
            double sum = 0;
            double r = 0;
            for (int j = 0; j < m; j++) {
                sum += q[j] * mtr[i][j];
                r += q[j] * risk[i][j];
            }
            expValues[i] = sum;
            avgRisk[i] = r;
        }
        double maxExpValue = expValues[0];
        double minRisk = avgRisk[0];
        int str1Num = 0;
        for (int i = 1; i < n; i++) {
            if (maxExpValue < expValues[i]) {
                maxExpValue = expValues[i];
                str1Num = i;
            }
            if (minRisk > avgRisk[i]) {
                minRisk = avgRisk[i];
            }
        }
        //вывести максимальное мат. ожидание выигрыша, стратегии, которые соответствуют ему

        System.out.println("Максимальное мат. ожидание выигрыша: " + maxExpValue);
        System.out.println("Минимальный риск: " + minRisk);
        System.out.println("Номер стратегии игрока: " + (str1Num + 1));
        System.out.println("==================");
        //максиминный критерий Вальда
        //нахождение максимина
        int[] minForRows = new int[n];
        for (int i = 0; i < n; i++) {
            int rMin = mtr[i][0];
            for (int j = 1; j < m; j++) {
                if (mtr[i][j] < rMin) {
                    rMin = mtr[i][j];
                }
            }
            minForRows[i] = rMin;
        }
        int maximin = minForRows[0];
        int optimA = 0;
        for (int i = 1; i < n; i++) {
            if (maximin < minForRows[i]) {
                maximin = minForRows[i];
                optimA = i;
            }
        }

        System.out.println("=======Критерий Вальда=======");
        System.out.println("Значение выигрыша: " + maximin);
        System.out.println("Номер оптимальной стратегии: " + (optimA + 1));
        //критерий севиджа
        int[] maxForRisks = new int[n];
        for (int i = 0; i < n; i++) {
            int rMin = mtr[i][0];
            for (int j = 1; j < m; j++) {
                if (risk[i][j] > rMin) {
                    rMin = risk[i][j];
                }
            }
            maxForRisks[i] = rMin;
        }
        int minimax = minForRows[0];
        optimA = 0;
        for (int i = 1; i < n; i++) {
            if (maximin > minForRows[i]) {
                maximin = minForRows[i];
                optimA = i;
            }
        }

        System.out.println("=======Критерий Севиджа=======");
        System.out.println("Значение риска: " + minimax);
        System.out.println("Номер оптимальной стратегии: " + (optimA + 1));

        //критерий гурвица
        if (xi >= 0 && xi <= 1) {
            double[] hi = new double[n];
            for (int i = 0; i < n; i++) {
                hi[i] = (1 - xi) * findMax(mtr[i], m) + xi * findMin(mtr[i], m);
            }
            optimA = 0;
            double resHi = hi[0];
            for (int i = 1; i < n; i++) {
                if (resHi < hi[i]) {
                    resHi = hi[i];
                    optimA = i;
                }
            }

            System.out.println("=======Критерий Гурвицв=======");
            System.out.println("Значение критерия: " + resHi);
            System.out.println("Номер оптимальной стратегии: " + (optimA + 1));
        } else {
            System.out.println("Коэффициент критерия Гурвица не принадлежит [0;1]");
        }
    }
}
