package lab3;


import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileNotFoundException;

public class lab3 extends Application {

    private Label TextKey;
    private Group TreePanel;
    private Group StagePanel;
    private Scene scene;
    private VarTree T;
    private double SY = 15;
    private double SX = 15;

    public static void main(String[] args) {
        launch(args);
    }

    private void TNew() {
        System.out.println("New Tree");
        try {
            T = new VarTree(new File("src/lab3/test"));
        } catch (FileNotFoundException e) {
            System.out.println("errr");
        }
        TextKey.setText(T.getRoot().path);
        T.setSX(SX);
        T.setSY(SY);
        refreshTree();
    }

    private void refreshTree() {
        T.refreshXY();
        TreePanel.getChildren().clear();
        if (T.getRoot() != null)
            refreshTree(T.getRoot());
    }

    private void refreshTree(TSP t) {

        TreePanel.getChildren().addAll(t.getShape());
        if (t.TryLeft())
            refreshTree(t.getLeft());
        if (t.TryRight())
            refreshTree(t.getRight());
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        StagePanel = new Group();

        TreePanel = new Group();
        TreePanel.setLayoutX(0);
        TreePanel.setLayoutY(60);

        TextKey = new Label();
        TextKey.setMaxWidth(400);

        TNew();

        StagePanel.getChildren().add(TextKey);
        StagePanel.getChildren().add(TreePanel);
        scene = new Scene(new ScrollPane(StagePanel), 600, 300);
        primaryStage.setTitle("TSPTree");
        primaryStage.setScene(scene);
        primaryStage.show();

    }
}