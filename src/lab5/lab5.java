package lab5;

/**
 * Created by idib on 07.05.17.
 */
public class lab5 {
    private static int lambda = 50;
    private static int mu = 12;

    private static int maxQueue = 15;
    private static int countThread = 5;

    private static int countWorkClockinDay = 6;
    private static int countWorkDayinMounth = 18;

    private static int salary = 15000;
    private static int income = 18;

    public static void main(String[] args) {
        double p = 1. * lambda / mu;
        System.out.println(1 + ") = " + (1 - p));
        System.out.println(2 + ") = " + (p * p / (1 - p)));
        System.out.println(3 + ") = " + (p / (mu * (1 - p))));
        System.out.println(4 + ") = " + (1 / (mu * (1 - p))));
        System.out.println(5 + ") = " + (p));
        System.out.println(6 + ") = " + (Math.pow(p, 11) / (1 - p)));

        int n = countThread;
        int m = maxQueue;


        double A = lambda * (1 - pRequest(p, p0(p, n, m), n, m));
        pritMonth(A);
    }

    private static double p0(double p, int n, int m) {
        double sum = 1;
        long fr = 1;
        double pn = 1;
        for (int i = 1; i < n; i++) {
            pn *= p;
            fr *= i;
            sum += pn / fr;
        }
        p /= n;
        sum += (p - Math.pow(p, m + 1)) / (1 - p);
        return 1 / sum;
    }

    private static double pRequest(double p, double p0, int n, int m) {
        double w = 1;
        for (int i = 1; i < n; i++) w *= i;
        return Math.pow(p, n + m) / Math.pow(n, m) / w * p0;
    }

    private static void pritWeek(double A) {
        System.out.println("чистая прибыл за неделю");
        System.out.println((int) (A * countWorkClockinDay) * income - countThread * salary / countWorkDayinMounth);
    }

    private static void pritMonth(double A) {
        System.out.println("чистая прибыл за месяц");
        System.out.println((int) (A * countWorkClockinDay) * countWorkDayinMounth * income - countThread *  salary );
    }
}
