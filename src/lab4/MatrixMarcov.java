package lab4;

import cMath.Func;
import cMath.FuncOnPoint;
import cMath.Matrix;
import cMath.chart;
import javafx.geometry.Point2D;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


/**
 * Created by idib on 12.04.17.
 */

public class MatrixMarcov extends Matrix {
    List<Func> funcs = new ArrayList<>();

    public MatrixMarcov(int n, int m) {
        super(n, m);
    }

    public MatrixMarcov(int n, int m, float initValue) {
        super(n, m, initValue);
    }

    public MatrixMarcov(String pathFile) throws FileNotFoundException {
        this(0, 0);
        Scanner in = new Scanner(new File(pathFile));
        n = in.nextInt();
        init(n, n, 0);
        int a, b;
        float c;
        while (in.hasNextInt()) {
            a = in.nextInt() - 1;
            b = in.nextInt() - 1;
            c = in.nextFloat();
            mat[a][a] -= c;
            mat[b][a] += c;
        }
        in.close();
    }

    public void sol() throws Exception {
        System.out.println(toString());

        Matrix Pk = new Matrix(n, 1);
        Pk.setMat(0, 0, 1);
        Matrix PkLast = Pk;
        System.out.println(Pk);
        Matrix A = this;
        double h = 0.0001;
        int sh = 100;

        for (int i = 0; i < n; i++) {
            funcs.add(new FuncOnPoint("p" + i + "(t)"));
        }

        List<Matrix> mt = add(getE(n), mul(h, A)).Lambda(0.001);
        System.out.println(mt);

        int maxI= 0;
        do {
            PkLast = Pk;
            Pk = mul(add(getE(n), mul(h, A)), Pk);
            if (maxI % sh == 0)
                for (int j = 0; j < n; j++) {
                    ((FuncOnPoint) (funcs.get(j))).add(new Point2D(h * maxI, Pk.getMat(j, 0)));
                }
            maxI++;
        } while (MaxDifference(Pk,PkLast) >= eps);
        System.out.println(Pk);

        Matrix tt = clone();
        for (int i = 0; i < n; i++) {
            tt.setMat(n - 1, i, 1);
        }
        Matrix b = new Matrix(n, 1, 0);
        b.setMat(b.n - 1, 0, 1);
        System.out.println(tt.det());
        tt.transformTrapezoidWithMax();
        tt.addColl(b);
        System.out.println(tt.Gauss());

        chart.srun("", funcs, 0, h * --maxI);
    }
}
