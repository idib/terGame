package lab1;

import javafx.beans.property.SimpleStringProperty;

/**
 * Created by idib on 12.02.17.
 */
public class Works {
    public boolean to = true;
    private final SimpleStringProperty valI;
    private final SimpleStringProperty valA;
    private final SimpleStringProperty valB;
    private final SimpleStringProperty valC;
    public int indexInOrder;
    public int a;
    public int b;
    public int c;
    public long timeA;
    public long timeB;
    public long timeC;

    public Works(int indexInOrder, int a, int b) {
        this.indexInOrder = indexInOrder;
        this.a = a;
        this.b = b;
        valI = new SimpleStringProperty(String.valueOf(indexInOrder));
        valA = new SimpleStringProperty(String.valueOf(a));
        valB = new SimpleStringProperty(String.valueOf(b));
        valC = new SimpleStringProperty(String.valueOf(0));
    }

    public Works(int indexInOrder, int a, int b, int c) {
        to = false;
        this.indexInOrder = indexInOrder;
        this.a = a;
        this.b = b;
        this.c = c;
        valI = new SimpleStringProperty(String.valueOf(indexInOrder));
        valA = new SimpleStringProperty(String.valueOf(a));
        valB = new SimpleStringProperty(String.valueOf(b));
        valC = new SimpleStringProperty(String.valueOf(c));
    }

    @Override
    public String toString() {
        return "task №" + indexInOrder + " a:" + a + " b:" + b + " timeA:" + timeA + " timeB:" + timeB;
    }

    public Works clone(){
        Works r = new Works(indexInOrder,a,b,c);
        r.to = to;
        return r;
    }

    public String getValI() {
        return valI.get();
    }

    public void setValI(String valI) {
        this.valI.set(valI);
    }

    public String getValA() {
        return valA.get();
    }

    public void setValA(String valA) {
        this.valA.set(valA);
    }

    public String getValB() {
        return valB.get();
    }

    public void setValB(String valB) {
        this.valB.set(valB);
    }

    public String getValC() {
        return valC.get();
    }

    public void setValC(String valC) {
        this.valC.set(valC);
    }
}
