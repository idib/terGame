//package cMath;
//
//import net.objecthunter.exp4j.Expression;
//
///**
// * Created by idib on 23.03.17.
// */
//public class FuncEval extends Func {
//    public double eps = 1e-6f;
//    public double steps;
//    public int countIter = 0;
//    Expression e = null;
//    FuncEval der = null;
//
//    public FuncEval(String name, Expression e) {
//        super(name);
//        this.e = e;
//    }
//
//    public FuncEval(String name, Expression e, FuncEval derivative) {
//        super(name);
//        der = derivative;
//        this.e = e;
//    }
//
//    @Override
//    public double calc(double x) {
//        return (double) e.setVariable("x", x).evaluate();
//    }
//
//    public double derivative(double x) {
//        if (der == null)
//            return (calc(x + eps) - calc(x - eps)) / (2 * eps);
//        else
//            return der.calc(x);
//    }
//
//    public double findXSecant(double star, double end) {
//        //check
//        double next;
//        countIter = 0;
//        do {
//            countIter++;
//            next = star - (star - end) * calc(star) / (calc(star) - calc(end));
//            end = star;
//            star = next;
//        } while (Math.abs(star - end) > eps);
//        return star;
//    }
//
//    public double findXСhord(double star, double end) {
//        //check
//        double x = star, _x = end;
//        double nextX;
//        double next_X;
//        countIter = 0;
//        do {
//            countIter++;
//            nextX = x - calc(x) / (calc(_x) - calc(x)) * (_x - x);
//            next_X = _x - calc(_x) / derivative(_x);
//            x = nextX;
//            _x = next_X;
//        } while (Math.abs(_x - x) > eps);
//        return _x;
//    }
////
////    public List<Float> findXs(double star, double end) {
////        double sum = Float.POSITIVE_INFINITY, last;
////        double step = (end - star) * 2;
////        do {
////            step /= 2;
////        } while (CheckNullable( star,  end, step));
////
////        return integrateRectangle(star, end, step);
////    }
//
//    private boolean CheckNullable(double star, double end, double step) {
//        int n = (int) ((end - star) / step);
//        for (int i = 1; i < n; i++) {
//            double fn_ = calc(star+(step*(i-1)));
//            double fn = calc(star+(step*i));
//            if(fn < 0 && fn_ < 0 || fn > 0 && fn_ > 0)
//                return false;
//        }
//        return true;
//    }
//
//    public double integrateRectangle(double star, double end) {
//        double sum = Float.POSITIVE_INFINITY, last;
//        double step = (end - star);
//        do {
//            last = sum;
//            step /= 2;
//            sum = integrateRectangle(star, end, step);
//        } while (Math.abs(last - sum) >= eps);
//        steps = step;
//        return sum;
//    }
//
//    public double integrateRectangle(double star, double end, double step) {
//        int n = (int) ((end - star) / step);
//        double sum = 0;
//        countIter = 0;
//        for (int i = 1; i <= n; i++) {
//            sum += (calc(star + step * i));
//            countIter++;
//        }
//        return sum * step;
//    }
//
//    public double integrateSimpson(double star, double end) {
//        double sum = Float.POSITIVE_INFINITY, last;
//        double step = (end - star);
//        do {
//            last = sum;
//            step /= 2;
//            sum = integrateSimpson(star, end, step);
//        } while (Math.abs(last - sum) >= eps);
//        steps = step;
//        return sum;
//    }
//
//    public double integrateSimpson(double star, double end, double step) {
//        int n = (int) ((end - star) / step) - 1;
//        double sum = 0;
//        countIter = 0;
//        for (int i = 1; i < n; i += 2) {
//            countIter++;
//            sum += (calc(star + step * (i - 1)) + 4 * calc(star + step * i) + calc(star + step * (i + 1)));
//        }
//        return sum * step / 3;
//    }
//
//    public double integrateTrapezoid(double star, double end) {
//        double sum = Float.POSITIVE_INFINITY, last;
//        double step = (end - star);
//        do {
//            last = sum;
//            step /= 2;
//            sum = integrateTrapezoid(star, end, step);
//        } while (Math.abs(last - sum) >= eps);
//        steps = step;
//        return sum;
//    }
//
//    public double integrateTrapezoid(double star, double end, double step) {
//        int n = (int) ((end - star) / step);
//        double sum = 0;
//        countIter = 0;
//        for (int i = 1; i < n; i++) {
//            countIter++;
//            sum += (calc(star + step * i) + calc(star + step * (i - 1))) / 2;
//        }
//        return sum * step;
//    }
//}
